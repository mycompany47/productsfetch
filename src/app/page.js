"use client";
import 'tailwindcss/tailwind.css';
import React from "react";
import { motion, AnimatePresence } from "framer-motion";
console.log("motion", motion)
import axios from "axios";
import Link from "next/link";
import { Loader } from './component/Loader';
export function Index() {


  const [productsData, setProducts] = React.useState([])
  const [isLoad, setLoad] = React.useState(true);

  const getProducts = async () => {
    try {
      const res = await axios.get("https://fakestoreapi.com/products");
      console.log("res---->", res.data)
      setProducts(res.data)
      setTimeout(() => {
        setLoad(false)
      }, 2000)
    } catch (error) {
      console.log("error", error)
    }
  }

  React.useEffect(() => {
    getProducts()
  }, [])




  return (
      <div>

        {isLoad ? <Loader></Loader>
          :
          <div className="container mx-auto">
            <div className="grid grid-cols-1">
              <div className="bg-dark">
                <motion.div
                  intial={{ y: -250 }}
                  animate={{ y: -30 }}
                  transition={{ delay: 0.6, type: "spring" }}
                >
                  <h2 className="text-2xl my-11 font-bold tracking-tight text-center text-gray-900 heading">Our Products</h2>
                </motion.div>
                <div className="mt-10 grid grid-cols-1 productsgrid gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
                  {productsData.map((element) => {
                    console.log("id------------", element.id)
                    return (<>
  
                      <Link href={`/products/${element.id}`}>
                        <motion.div
                          intial={{ y: -250 }}
                          animate={{ y: -30 }}
                          transition={{ delay: 0.6, type: "spring" }}
                          key={element.id} className="group relative productcard">
                          <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-80">
                            <img
                              src={element.image}
                              alt="thumbnail"
                              className="h-full w-full object-cover object-center lg:h-full lg:w-full"
                            />
                          </div>
                          <div className="cardfooter">
                            <div className="mt-4 flex justify-between">
                              <div>
                                <h3 className="text-sm text-gray-700">
                                  <span aria-hidden="true" className="absolute inset-0" />
                                  {element.title}
                                </h3>
                                <p className="mt-10 text-sm text-white-500">{element.category}</p>
                              </div>
                              <p className="text-sm font-medium text-gray-900">₹{element.price}</p>
                            </div>
                          </div>
                        </motion.div>
                      </Link>
                    </>)
                  })}
                </div>
              </div>
            </div>
          </div>
        }
        
      </div>
  );
}

export default Index;