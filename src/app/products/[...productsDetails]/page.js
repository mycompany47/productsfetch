"use client";
import React, { useState, useEffect } from "react";
import Link from "next/link";
import { StarIcon } from '@heroicons/react/20/solid';
import { RadioGroup } from '@headlessui/react'
import { motion, AnimatePresence } from "framer-motion";
import "../../styles/productdetails.css";
import axios from "axios";
import Imageloader from "@/app/component/imageLoader";


export default function ProductDetails({ params }) {

    let id = params.productsDetails[0];
    console.log("id--------------", id)

    const [productDetail, setProductDetail] = useState([])
    const [isLoadImage, setLoadImage] = useState(true)


    const product = {
        name: 'Basic Tee 6-Pack',
        price: '$192',
        href: '#',
        breadcrumbs: [
            { id: 1, name: 'Men', href: '#' },
            { id: 2, name: 'Clothing', href: '#' },
        ],
        images: [
            {
                src: 'https://tailwindui.com/img/ecommerce-images/product-page-02-secondary-product-shot.jpg',
                alt: 'Two each of gray, white, and black shirts laying flat.',
            },
            {
                src: 'https://tailwindui.com/img/ecommerce-images/product-page-02-tertiary-product-shot-01.jpg',
                alt: 'Model wearing plain black basic tee.',
            },
            {
                src: 'https://tailwindui.com/img/ecommerce-images/product-page-02-tertiary-product-shot-02.jpg',
                alt: 'Model wearing plain gray basic tee.',
            },
            {
                src: 'https://tailwindui.com/img/ecommerce-images/product-page-02-featured-product-shot.jpg',
                alt: 'Model wearing plain white basic tee.',
            },
        ],
        colors: [
            { name: 'White', class: 'bg-white', selectedClass: 'ring-gray-400' },
            { name: 'Gray', class: 'bg-gray-200', selectedClass: 'ring-gray-400' },
            { name: 'Black', class: 'bg-gray-900', selectedClass: 'ring-gray-900' },
        ],
        sizes: [
            { name: 'XXS', inStock: false },
            { name: 'XS', inStock: true },
            { name: 'S', inStock: true },
            { name: 'M', inStock: true },
            { name: 'L', inStock: true },
            { name: 'XL', inStock: true },
            { name: '2XL', inStock: true },
            { name: '3XL', inStock: true },
        ],
        description:
            'The Basic Tee 6-Pack allows you to fully express your vibrant personality with three grayscale options. Feeling adventurous? Put on a heather gray tee. Want to be a trendsetter? Try our exclusive colorway: "Black". Need to add an extra pop of color to your outfit? Our white tee has you covered.',
        highlights: [
            'Hand cut and sewn locally',
            'Dyed with our proprietary colors',
            'Pre-washed & pre-shrunk',
            'Ultra-soft 100% cotton',
        ],
        details:
            'The 6-Pack includes two black, two white, and two heather gray Basic Tees. Sign up for our subscription service and be the first to get new, exciting colors, like our upcoming "Charcoal Gray" limited release.',
    }

    const getSingleProductDetail = async () => {
        let arr = [];
        try {
            const data = await axios.get(`https://fakestoreapi.com/products/${id}`)
            console.log("data---------", data.data)
            arr.push(data.data)
            setLoadImage(false)
            setProductDetail(arr)
        } catch (error) {
            console.log("eror", error)
        }
    }


    useEffect(() => {
        getSingleProductDetail()
        console.log("productDetail----", typeof productDetail)
    }, [])


    const reviews = { href: '#', average: 4, totalCount: 117 }

    function classNames(...classes) {
        return classes.filter(Boolean).join(' ')
    }



    return (
        <div className="backgroundBox">
            {isLoadImage ?

                <Imageloader></Imageloader>
                :
                <div className="container mx-auto">
                    <div className="pt-6">

                        <motion.div
                            intial={{ y: -250 }}
                            animate={{ y: -30 }}
                            transition={{ delay: 0.6, type: "spring" }}
                        >
                            <h1 className="text-2xl my-11 font-bold tracking-tight text-center text-gray-900 heading">Product Details</h1>
                        </motion.div>

                        <div className="mx-auto mt-6 max-w-2xl sm:px-6 lg:grid lg:max-w-7xl lg:grid-cols-3 lg:gap-x-8 lg:px-8">

                            {productDetail.map((element) => {
                                return (<>

                                    <motion.div
                                        initial={{ x: -250 }}
                                        animate={{ x: -10 }}
                                        className="aspect-h-4 aspect-w-3  overflow-hidden rounded-lg lg:block imageBox">
                                        <img
                                            src={element.image}
                                            alt="product image"
                                            className="h-full w-full object-cover object-center"
                                        />
                                    </motion.div>

                                    <motion.div
                                        initial={{ x: -250 }}
                                        animate={{ x: -10 }}
                                        className="aspect-h-4 aspect-w-3 hidden overflow-hidden rounded-lg lg:block imageBox">
                                        <img
                                            src={element.image}
                                            alt="product image"
                                            className="h-full w-full object-cover object-center"
                                        />
                                    </motion.div>

                                </>)
                            })

                            }


                        </div>
                        {/* Product info */}

                        {productDetail.map((element, index) => {
                            console.log("eleme---------", element.id)
                            return (<>

                                <motion.div
                                    initial={{ x: 250 }}
                                    animate={{ x: 10 }}
                                    className="mx-auto max-w-2xl px-4 pb-16 pt-10 sm:px-6 lg:grid lg:max-w-7xl lg:grid-cols-3 lg:grid-rows-[auto,auto,1fr] lg:gap-x-8 lg:px-8 lg:pb-24 lg:pt-16">
                                    <div className="lg:col-span-2 lg:border-r lg:border-gray-200 lg:pr-8 text">
                                        <h1 className="text-2xl font-bold tracking-tight text-gray-900 sm:text-3xl">{element.title}</h1>
                                    </div>

                                    {/* Options */}
                                    <div className="mt-4 lg:row-span-3 lg:mt-0">
                                        <p className="text-3xl tracking-tight text-gray-900">₹{element.price}</p>

                                        {/* Reviews */}
                                        <div className="mt-6">
                                            <div className="flex items-center">
                                                <div className="flex items-center">
                                                    {Array.from({ length: element.rating.rate }, (_, index) => index + 1).map((rating) => (
                                                        <StarIcon
                                                            key={rating}
                                                            className={classNames(
                                                                reviews.average > rating ? 'text-yellow-900' : 'text-gray-200',
                                                                'h-5 w-5 flex-shrink-0'
                                                            )}
                                                            aria-hidden="true"
                                                        />
                                                    ))}
                                                </div>
                                                <p>&nbsp;&nbsp;{element.rating.count} reviews</p>
                                                
                                            </div>
                                        </div>
                 
                                    </div>

                                    <div className="py-10 lg:col-span-2 lg:col-start-1 lg:border-r lg:border-gray-200 lg:pb-16 lg:pr-8 lg:pt-6">
                                        {/* Description and details */}
                                        <div>
                                            <h3 className="sr-only">Description</h3>

                                            <div className="space-y-6">
                                                <p className="text-base text-gray-900">{element.description}</p>
                                            </div>
                                        </div>

                                        <div className="mt-10">
                                            <h2 className="text-sm font-medium text-gray-900">Category</h2>

                                            <div className="mt-4 space-y-6">
                                                <p className="text-sm text-gray-600">{element.category}</p>
                                            </div>
                                        </div>
                                    </div>
                                </motion.div>


                            </>)
                        })

                        }

                    </div>


                </div>
            }
        </div>
    );


}
